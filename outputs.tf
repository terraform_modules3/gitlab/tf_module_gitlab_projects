output "projects" {
  value = local.projects
}
output "runner_token" {
  value     = local.runner_token
  sensitive = true
}
