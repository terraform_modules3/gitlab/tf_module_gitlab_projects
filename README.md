# Gitlab projects Terraform module
Module to create and manage projects in Gitlab

Allows to add branch rules, webhooks, deploy_keys

Does NOT support creation or management of project's push rules

## Usage example
```hcl
module "projects" {
  source = "git::https://gitlab.com/terraform_modules3/gitlab/tf_module_gitlab_projects.git?ref=0.1.0"

  projects = [
    { 0 = {
      name                   = "repo-name",
      parent_group_full_path = "group1/subgroup1"
      branch_rules = [
        { branch             = "dev"
          merge_access_level = "developer"
          push_access_level  = "developer"
        },
        { branch             = "main"
          merge_access_level = "maintainer"
          push_access_level  = "maintainer"
        },
      ]
      webhooks = [
        {
          url = "https://test.example.com"
        },
      ]
      deploy_keys = [
        {
          key_id = 1
        },
      ]
      runners = [
        { 1 = {
          locked          = false
          maximum_timeout = 600
          paused          = false
          access_level    = "not_protected" # not_protected, ref_protected.
          description     = "Docker executor"
          tag_list        = ["docker"]
          untagged        = true
        } },
        { 2 = {} },
      ]
      env_vars = [
        { 1 = {
          key               = "VAR1"
          value             = var.var1_secret_value
          masked            = true
        } },
      ]
    } },
    { 1 = {
      name                                             = "repo-name2",
      namespace_id                                     = 1
      default_branch                                   = "develop"
      initialize_with_readme                           = false
      merge_method                                     = "ff"
      only_allow_merge_if_all_discussions_are_resolved = true
      ci_config_path                                   = .ci/.gitlab-ci.yml
    } },
    { 2 = {
      name                   = "repo-name3",
      parent_group_full_path = "group1"
      archived = true
    } },
  ]

  providers = {
    gitlab = gitlab
  }
}

```

## Output example
```hcl
projects = {
  "group1/subgroup1/repo-name" = {
    "default_branch" = "main"
    "full_name" = "group1/subgroup1/repo-name"
    "http_clone" = "https://gitlab.example.com/group1/subgroup1/repo-name.git"
    "id" = "7"
    "name" = "repo-name"
    "ssh_clone" = "git@gitlab.example.com:group1/subgroup1/repo-name.git"
    "visibility" = "private"
  }
  "repo-name2" = {
    "default_branch" = "develop"
    "full_name" = "repo-name2"
    "http_clone" = "https://gitlab.example.com/repo-name2.git"
    "id" = "8"
    "name" = "repo-name2"
    "ssh_clone" = "git@gitlab.example.com:wtrg/mailgod/repo-name2.git"
    "visibility" = "private"
  }
  "group1/repo-name3" = {
    "default_branch" = "main"
    "full_name" = "group1/repo-name3"
    "http_clone" = "https://gitlab.example.com/group1/repo-name3.git"
    "id" = "2"
    "name" = "repo-name3"
    "ssh_clone" = "git@gitlab.example.com:group1/repo-name3.git"
    "visibility" = "private"
  }
}
```

### if «path» property is omitted, it will be generated from «name» property by lowercasing and replacing whitespaces with underscore symbol
```console
lower(replace(each.value.name, "/\\s/", "_"))
```
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.7 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 16.9.1, < 17.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | 16.11.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_branch_protection.branch](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection) | resource |
| [gitlab_deploy_key_enable.key](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_key_enable) | resource |
| [gitlab_project.project](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project) | resource |
| [gitlab_project_hook.webhook](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_hook) | resource |
| [gitlab_project_variable.variable](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_user_runner.project_runner](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/user_runner) | resource |
| [gitlab_group.group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_default_namespace_id"></a> [default\_namespace\_id](#input\_default\_namespace\_id) | curl -s --header "PRIVATE-TOKEN: $GITLAB\_TOKEN" "https://gitlab.com/api/v4/namespaces/<namespace-name>" \|jq .id | `number` | `0` | no |
| <a name="input_projects"></a> [projects](#input\_projects) | https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project<br><br>  Notes:<br>  If namespace\_id is specified, parent\_group\_full\_path is ignored<br>  The allowed\_to\_push, allowed\_to\_merge, allowed\_to\_unprotect, unprotect\_access\_level and code\_owner\_approval\_required in branch\_rules attributes works only in GitLab Enterprise instance.<br><br>  Example:<br>    projects = [<br>      { 0 = {<br>        name                   = "repo-name",<br>        parent\_group\_full\_path = "group/group2"<br>        branch\_rules = [<br>          { branch             = "dev"<br>            merge\_access\_level = "developer"<br>            push\_access\_level  = "developer"<br>          },<br>          { branch             = "main"<br>            merge\_access\_level = "maintainer"<br>            push\_access\_level  = "maintainer"<br>          },<br>        ]<br>        webhooks = [<br>          {<br>            url = "https://test.example.com"<br>          },<br>        ]<br>        deploy\_keys = [<br>          {<br>            key\_id = 1<br>          },<br>        ]<br>        runners = [<br>          { 1 = {<br>            locked          = false<br>            maximum\_timeout = 600<br>            paused          = false<br>            access\_level    = "not\_protected" # not\_protected, ref\_protected.<br>            description     = "Docker executor"<br>            tag\_list        = ["docker"]<br>            untagged        = true<br>          } },<br>          { 2 = {} },<br>        ]<br>        env\_vars = [<br>          { 1 = {<br>            key               = "VAR1"<br>            value             = var.var1\_secret\_value<br>            masked            = true<br>          } },<br>        ]<br>      } },<br>      { 1 = {<br>        name                                             = "repo-name2",<br>        namespace\_id                                     = 3<br>        default\_branch                                   = "develop"<br>        initialize\_with\_readme                           = false<br>        merge\_method                                     = "ff"<br>        only\_allow\_merge\_if\_all\_discussions\_are\_resolved = true<br>        ci\_config\_path                                   = .ci/.gitlab-ci.yml<br>      } },<br>      { 2 = {<br>        name                   = "repo-name3",<br>        parent\_group\_full\_path = "group"<br>        archived = true<br>      } },<br>    ] | <pre>list(map(<br>    object({<br>      name = string<br><br>      namespace_id           = optional(number)<br>      parent_group_full_path = optional(string)<br><br>      allow_merge_on_skipped_pipeline                  = optional(bool, false)<br>      analytics_access_level                           = optional(string, "enabled") # disabled, private, enabled<br>      archive_on_destroy                               = optional(bool, false)<br>      archived                                         = optional(bool, false)<br>      auto_cancel_pending_pipelines                    = optional(string, "enabled")    # disabled, enabled<br>      auto_devops_deploy_strategy                      = optional(string, "continuous") # continuous, manual, timed_incremental<br>      auto_devops_enabled                              = optional(bool, false)<br>      autoclose_referenced_issues                      = optional(bool, true)<br>      avatar                                           = optional(string, "")<br>      avatar_hash                                      = optional(string, "")<br>      build_git_strategy                               = optional(string, "fetch")<br>      build_timeout_seconds                            = optional(number, 3600)<br>      builds_access_level                              = optional(string, "enabled") # disabled, private, enabled<br>      ci_config_path                                   = optional(string, "")<br>      ci_default_git_depth                             = optional(number, 1)<br>      ci_forward_deployment_enabled                    = optional(bool, true)<br>      ci_separated_caches                              = optional(bool, true)<br>      container_registry_access_level                  = optional(string, "enabled") # disabled, private, enabled<br>      default_branch                                   = optional(string, "main")<br>      description                                      = optional(string, "")<br>      emails_disabled                                  = optional(bool, false)<br>      environments_access_level                        = optional(string, "enabled") # disabled, private, enabled<br>      external_authorization_classification_label      = optional(string, "")<br>      feature_flags_access_level                       = optional(string, "enabled") # disabled, private, enabled<br>      forked_from_project_id                           = optional(number, null)<br>      forking_access_level                             = optional(string, "enabled") # disabled, private, enabled<br>      group_with_project_templates_id                  = optional(number, null)<br>      import_url                                       = optional(string)<br>      import_url_password                              = optional(string)<br>      import_url_username                              = optional(string)<br>      infrastructure_access_level                      = optional(string, "enabled") # disabled, private, enabled<br>      initialize_with_readme                           = optional(bool, true)<br>      issues_access_level                              = optional(string, "enabled") # disabled, private, enabled<br>      issues_enabled                                   = optional(bool, true)<br>      keep_latest_artifact                             = optional(bool, true)<br>      lfs_enabled                                      = optional(bool, true)<br>      merge_commit_template                            = optional(string, "")<br>      merge_method                                     = optional(string, "rebase_merge") # merge, rebase_merge, ff<br>      merge_pipelines_enabled                          = optional(bool, false)<br>      merge_requests_access_level                      = optional(string, "enabled") # disabled, private, enabled<br>      merge_requests_enabled                           = optional(bool, true)<br>      merge_requests_template                          = optional(string, "")<br>      merge_trains_enabled                             = optional(bool, false)<br>      mirror                                           = optional(bool)<br>      mirror_overwrites_diverged_branches              = optional(bool)<br>      mirror_trigger_builds                            = optional(bool)<br>      monitor_access_level                             = optional(string, "enabled") # disabled, private, enabled<br>      mr_default_target_self                           = optional(bool)<br>      only_allow_merge_if_all_discussions_are_resolved = optional(bool, false)<br>      only_allow_merge_if_pipeline_succeeds            = optional(bool, false)<br>      only_mirror_protected_branches                   = optional(bool)<br>      packages_enabled                                 = optional(bool, true)<br>      pages_access_level                               = optional(string, "private")<br>      path                                             = optional(string, "")<br>      printing_merge_request_link_enabled              = optional(bool, true)<br>      public_builds                                    = optional(bool, false)<br>      releases_access_level                            = optional(string, "enabled") # disabled, private, enabled<br>      remove_source_branch_after_merge                 = optional(bool, true)<br>      repository_access_level                          = optional(string, "enabled") # disabled, private, enabled<br>      repository_storage                               = optional(string)<br>      request_access_enabled                           = optional(bool, true)<br>      resolve_outdated_diff_discussions                = optional(bool, false)<br>      restrict_user_defined_variables                  = optional(bool, false)<br>      security_and_compliance_access_level             = optional(string, "private") # disabled, private, enabled<br>      shared_runners_enabled                           = optional(bool, false)<br>      skip_wait_for_default_branch_protection          = optional(bool, true)<br>      snippets_access_level                            = optional(string, "enabled") # disabled, private, enabled<br>      snippets_enabled                                 = optional(bool, true)<br>      squash_commit_template                           = optional(string, "")<br>      squash_option                                    = optional(string, "default_off") # never, always, default_on, default_off<br>      suggestion_commit_message                        = optional(string, "")<br>      tags                                             = optional(list(string), [])<br>      template_name                                    = optional(string, "")<br>      template_project_id                              = optional(number, null)<br>      topics                                           = optional(list(string), [])<br>      use_custom_template                              = optional(bool)<br>      visibility_level                                 = optional(string, "private") # private, public<br>      wiki_access_level                                = optional(string, "enabled") # disabled, private, enabled<br>      wiki_enabled                                     = optional(bool, true)<br><br>      container_expiration_policy = optional(object({<br>        cadence           = string<br>        enabled           = bool # true,false<br>        keep_n            = number<br>        name_regex_delete = string<br>        name_regex_keep   = string<br>        older_than        = string<br>        }), {<br>        cadence           = "1d"  #  1d, 7d, 14d, 1month, 3month<br>        enabled           = false # true,false<br>        keep_n            = 10<br>        name_regex_delete = ".*"<br>        name_regex_keep   = ".*"<br>        older_than        = "90d"<br>      })<br><br>      branch_rules = optional(list(<br>        object({<br>          branch                       = string<br>          allow_force_push             = optional(bool, false)<br>          merge_access_level           = optional(string, "maintainer")<br>          push_access_level            = optional(string, "maintainer")<br>          unprotect_access_level       = optional(string, "maintainer")<br>          code_owner_approval_required = optional(bool, false)<br>          users_allowed_to_push        = optional(list(number), [])<br>          users_allowed_to_merge       = optional(list(number), [])<br>          users_allowed_to_unprotect   = optional(list(number), [])<br>          groups_allowed_to_push       = optional(list(number), [])<br>          groups_allowed_to_merge      = optional(list(number), [])<br>          groups_allowed_to_unprotect  = optional(list(number), [])<br>      })), [])<br><br>      webhooks = optional(list(<br>        object({<br>          url                        = string<br>          enable_ssl_verification    = optional(bool, true)<br>          push_events                = optional(bool, true)<br>          push_events_branch_filter  = optional(string)<br>          confidential_issues_events = optional(bool, false)<br>          confidential_note_events   = optional(bool, false)<br>          deployment_events          = optional(bool, false)<br>          issues_events              = optional(bool, false)<br>          job_events                 = optional(bool, false)<br>          merge_requests_events      = optional(bool, false)<br>          note_events                = optional(bool, false)<br>          pipeline_events            = optional(bool, false)<br>          releases_events            = optional(bool, false)<br>          tag_push_events            = optional(bool, false)<br>          wiki_page_events           = optional(bool, false)<br>      })), [])<br><br>      deploy_keys = optional(list(<br>        object({<br>          key_id   = number<br>          can_push = optional(bool, false)<br>      })), [])<br><br>      runners = optional(list(map(object({<br>        locked          = optional(bool, null)<br>        maximum_timeout = optional(number, null)<br>        paused          = optional(bool, null)<br>        access_level    = optional(string, null) # not_protected, ref_protected.<br>        description     = optional(string, null)<br>        tag_list        = optional(list(string), null)<br>        untagged        = optional(bool, null)<br>      }))), [])<br><br>      env_vars = optional(list(map(object({<br>        key               = string<br>        value             = string<br>        description       = optional(string, null)<br>        environment_scope = optional(string, null) # not_protected, ref_protected.<br>        masked            = optional(bool, null)<br>        raw               = optional(bool, null)<br>        protected         = optional(bool, null)<br>        variable_type     = optional(string, null) # env_var | file<br>      }))), [])<br><br>    })<br>  ))</pre> | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_projects"></a> [projects](#output\_projects) | n/a |
| <a name="output_runner_token"></a> [runner\_token](#output\_runner\_token) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
