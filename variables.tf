variable "default_namespace_id" {
  description = <<EOT
  curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/namespaces/<namespace-name>" |jq .id
  EOT

  type    = number
  default = 0
}

variable "projects" {
  description = <<EOT
  https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project

  Notes:
  If namespace_id is specified, parent_group_full_path is ignored
  The allowed_to_push, allowed_to_merge, allowed_to_unprotect, unprotect_access_level and code_owner_approval_required in branch_rules attributes works only in GitLab Enterprise instance.

  Example:
    projects = [
      { 0 = {
        name                   = "repo-name",
        parent_group_full_path = "group/group2"
        branch_rules = [
          { branch             = "dev"
            merge_access_level = "developer"
            push_access_level  = "developer"
          },
          { branch             = "main"
            merge_access_level = "maintainer"
            push_access_level  = "maintainer"
          },
        ]
        webhooks = [
          {
            url = "https://test.example.com"
          },
        ]
        deploy_keys = [
          {
            key_id = 1
          },
        ]
        runners = [
          { 1 = {
            locked          = false
            maximum_timeout = 600
            paused          = false
            access_level    = "not_protected" # not_protected, ref_protected.
            description     = "Docker executor"
            tag_list        = ["docker"]
            untagged        = true
          } },
          { 2 = {} },
        ]
        env_vars = [
          { 1 = {
            key               = "VAR1"
            value             = var.var1_secret_value
            masked            = true
          } },
        ]
      } },
      { 1 = {
        name                                             = "repo-name2",
        namespace_id                                     = 3
        default_branch                                   = "develop"
        initialize_with_readme                           = false
        merge_method                                     = "ff"
        only_allow_merge_if_all_discussions_are_resolved = true
        ci_config_path                                   = .ci/.gitlab-ci.yml
      } },
      { 2 = {
        name                   = "repo-name3",
        parent_group_full_path = "group"
        archived = true
      } },
    ]
  EOT
  type = list(map(
    object({
      name = string

      namespace_id           = optional(number)
      parent_group_full_path = optional(string)

      allow_merge_on_skipped_pipeline                  = optional(bool, false)
      analytics_access_level                           = optional(string, "enabled") # disabled, private, enabled
      archive_on_destroy                               = optional(bool, false)
      archived                                         = optional(bool, false)
      auto_cancel_pending_pipelines                    = optional(string, "enabled")    # disabled, enabled
      auto_devops_deploy_strategy                      = optional(string, "continuous") # continuous, manual, timed_incremental
      auto_devops_enabled                              = optional(bool, false)
      autoclose_referenced_issues                      = optional(bool, true)
      avatar                                           = optional(string, "")
      avatar_hash                                      = optional(string, "")
      build_git_strategy                               = optional(string, "fetch")
      build_timeout_seconds                            = optional(number, 3600)
      builds_access_level                              = optional(string, "enabled") # disabled, private, enabled
      ci_config_path                                   = optional(string, "")
      ci_default_git_depth                             = optional(number, 1)
      ci_forward_deployment_enabled                    = optional(bool, true)
      ci_separated_caches                              = optional(bool, true)
      container_registry_access_level                  = optional(string, "enabled") # disabled, private, enabled
      default_branch                                   = optional(string, "main")
      description                                      = optional(string, "")
      emails_disabled                                  = optional(bool, false)
      environments_access_level                        = optional(string, "enabled") # disabled, private, enabled
      external_authorization_classification_label      = optional(string, "")
      feature_flags_access_level                       = optional(string, "enabled") # disabled, private, enabled
      forked_from_project_id                           = optional(number, null)
      forking_access_level                             = optional(string, "enabled") # disabled, private, enabled
      group_with_project_templates_id                  = optional(number, null)
      import_url                                       = optional(string)
      import_url_password                              = optional(string)
      import_url_username                              = optional(string)
      infrastructure_access_level                      = optional(string, "enabled") # disabled, private, enabled
      initialize_with_readme                           = optional(bool, true)
      issues_access_level                              = optional(string, "enabled") # disabled, private, enabled
      issues_enabled                                   = optional(bool, true)
      keep_latest_artifact                             = optional(bool, true)
      lfs_enabled                                      = optional(bool, true)
      merge_commit_template                            = optional(string, "")
      merge_method                                     = optional(string, "rebase_merge") # merge, rebase_merge, ff
      merge_pipelines_enabled                          = optional(bool, false)
      merge_requests_access_level                      = optional(string, "enabled") # disabled, private, enabled
      merge_requests_enabled                           = optional(bool, true)
      merge_requests_template                          = optional(string, "")
      merge_trains_enabled                             = optional(bool, false)
      mirror                                           = optional(bool)
      mirror_overwrites_diverged_branches              = optional(bool)
      mirror_trigger_builds                            = optional(bool)
      monitor_access_level                             = optional(string, "enabled") # disabled, private, enabled
      mr_default_target_self                           = optional(bool)
      only_allow_merge_if_all_discussions_are_resolved = optional(bool, false)
      only_allow_merge_if_pipeline_succeeds            = optional(bool, false)
      only_mirror_protected_branches                   = optional(bool)
      packages_enabled                                 = optional(bool, true)
      pages_access_level                               = optional(string, "private")
      path                                             = optional(string, "")
      printing_merge_request_link_enabled              = optional(bool, true)
      public_builds                                    = optional(bool, false)
      releases_access_level                            = optional(string, "enabled") # disabled, private, enabled
      remove_source_branch_after_merge                 = optional(bool, true)
      repository_access_level                          = optional(string, "enabled") # disabled, private, enabled
      repository_storage                               = optional(string)
      request_access_enabled                           = optional(bool, true)
      resolve_outdated_diff_discussions                = optional(bool, false)
      restrict_user_defined_variables                  = optional(bool, false)
      security_and_compliance_access_level             = optional(string, "private") # disabled, private, enabled
      shared_runners_enabled                           = optional(bool, false)
      skip_wait_for_default_branch_protection          = optional(bool, true)
      snippets_access_level                            = optional(string, "enabled") # disabled, private, enabled
      snippets_enabled                                 = optional(bool, true)
      squash_commit_template                           = optional(string, "")
      squash_option                                    = optional(string, "default_off") # never, always, default_on, default_off
      suggestion_commit_message                        = optional(string, "")
      tags                                             = optional(list(string), [])
      template_name                                    = optional(string, "")
      template_project_id                              = optional(number, null)
      topics                                           = optional(list(string), [])
      use_custom_template                              = optional(bool)
      visibility_level                                 = optional(string, "private") # private, public
      wiki_access_level                                = optional(string, "enabled") # disabled, private, enabled
      wiki_enabled                                     = optional(bool, true)

      container_expiration_policy = optional(object({
        cadence           = string
        enabled           = bool # true,false
        keep_n            = number
        name_regex_delete = string
        name_regex_keep   = string
        older_than        = string
        }), {
        cadence           = "1d"  #  1d, 7d, 14d, 1month, 3month
        enabled           = false # true,false
        keep_n            = 10
        name_regex_delete = ".*"
        name_regex_keep   = ".*"
        older_than        = "90d"
      })

      branch_rules = optional(list(
        object({
          branch                       = string
          allow_force_push             = optional(bool, false)
          merge_access_level           = optional(string, "maintainer")
          push_access_level            = optional(string, "maintainer")
          unprotect_access_level       = optional(string, "maintainer")
          code_owner_approval_required = optional(bool, false)
          users_allowed_to_push        = optional(list(number), [])
          users_allowed_to_merge       = optional(list(number), [])
          users_allowed_to_unprotect   = optional(list(number), [])
          groups_allowed_to_push       = optional(list(number), [])
          groups_allowed_to_merge      = optional(list(number), [])
          groups_allowed_to_unprotect  = optional(list(number), [])
      })), [])

      webhooks = optional(list(
        object({
          url                        = string
          enable_ssl_verification    = optional(bool, true)
          push_events                = optional(bool, true)
          push_events_branch_filter  = optional(string)
          confidential_issues_events = optional(bool, false)
          confidential_note_events   = optional(bool, false)
          deployment_events          = optional(bool, false)
          issues_events              = optional(bool, false)
          job_events                 = optional(bool, false)
          merge_requests_events      = optional(bool, false)
          note_events                = optional(bool, false)
          pipeline_events            = optional(bool, false)
          releases_events            = optional(bool, false)
          tag_push_events            = optional(bool, false)
          wiki_page_events           = optional(bool, false)
      })), [])

      deploy_keys = optional(list(
        object({
          key_id   = number
          can_push = optional(bool, false)
      })), [])

      runners = optional(list(map(object({
        locked          = optional(bool, null)
        maximum_timeout = optional(number, null)
        paused          = optional(bool, null)
        access_level    = optional(string, null) # not_protected, ref_protected.
        description     = optional(string, null)
        tag_list        = optional(list(string), null)
        untagged        = optional(bool, null)
      }))), [])

      env_vars = optional(list(map(object({
        key               = string
        value             = string
        description       = optional(string, null)
        environment_scope = optional(string, null) # not_protected, ref_protected.
        masked            = optional(bool, null)
        raw               = optional(bool, null)
        protected         = optional(bool, null)
        variable_type     = optional(string, null) # env_var | file
      }))), [])

    })
  ))

  validation {
    condition = alltrue([
      for project in var.projects :
      can(
        values(project)[0].namespace_id != null ||
        values(project)[0].parent_group_full_path != null
      )
    ])
    error_message = "Either namespace_id or parent_group_full_path must have a value"
  }
  default = []
}
