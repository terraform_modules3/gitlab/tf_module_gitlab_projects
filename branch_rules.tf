locals {
  branch_rules = toset(flatten([
    for project in var.projects : [
      for rule in values(project)[0].branch_rules : merge(
        { project_index = keys(project)[0] },
      rule)
    ]
  ]))
}
resource "gitlab_branch_protection" "branch" {
  for_each = { for rule in local.branch_rules : "${rule.project_index}:${rule.branch}" => rule }

  branch                       = each.value.branch
  allow_force_push             = each.value.allow_force_push
  merge_access_level           = each.value.merge_access_level
  push_access_level            = each.value.push_access_level
  unprotect_access_level       = each.value.unprotect_access_level
  code_owner_approval_required = each.value.code_owner_approval_required
  project                      = gitlab_project.project[each.value.project_index].id

  depends_on = [gitlab_project.project]

  dynamic "allowed_to_push" {
    for_each = each.value.users_allowed_to_push
    content {
      user_id = allowed_to_push.value
    }
  }

  dynamic "allowed_to_merge" {
    for_each = each.value.users_allowed_to_merge
    content {
      user_id = allowed_to_merge.value
    }
  }

  dynamic "allowed_to_unprotect" {
    for_each = each.value.users_allowed_to_unprotect
    content {
      user_id = allowed_to_unprotect.value
    }
  }

  dynamic "allowed_to_push" {
    for_each = each.value.groups_allowed_to_push
    content {
      group_id = allowed_to_push.value
    }
  }

  dynamic "allowed_to_merge" {
    for_each = each.value.groups_allowed_to_merge
    content {
      group_id = allowed_to_merge.value
    }
  }

  dynamic "allowed_to_unprotect" {
    for_each = each.value.groups_allowed_to_unprotect
    content {
      group_id = allowed_to_unprotect.value
    }
  }
}
