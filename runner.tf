resource "gitlab_user_runner" "project_runner" {
  for_each = { for runner in local.runners : "${runner.project_index}:${runner.runner_index}" => runner }

  runner_type = "project_type"
  project_id  = gitlab_project.project[each.value.project_index].id

  locked          = each.value.locked
  maximum_timeout = each.value.maximum_timeout
  paused          = each.value.paused
  access_level    = each.value.access_level
  description     = each.value.description
  tag_list        = each.value.tag_list
  untagged        = each.value.untagged

  depends_on = [
    gitlab_project.project
  ]
}

locals {
  runners = flatten([
    for project in var.projects : [
      for runner in project[keys(project)[0]].runners : {
        project_index   = keys(project)[0]
        runner_index    = keys(runner)[0]
        locked          = runner[keys(runner)[0]].locked
        maximum_timeout = runner[keys(runner)[0]].maximum_timeout
        paused          = runner[keys(runner)[0]].paused
        access_level    = runner[keys(runner)[0]].access_level
        description     = runner[keys(runner)[0]].description
        tag_list        = runner[keys(runner)[0]].tag_list
        untagged        = runner[keys(runner)[0]].untagged
      }
    ]
  ])
}

locals {
  # for output
  runner_token = {
    for index, runner_info in gitlab_user_runner.project_runner :
    index => {
      token = runner_info.token
    }
  }
}
