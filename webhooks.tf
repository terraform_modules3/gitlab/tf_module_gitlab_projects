
locals {
  webhooks = toset(flatten([
    for project in var.projects : [
      for webhook in values(project)[0].webhooks : merge(
        { project_index = keys(project)[0] },
      webhook)
    ]
  ]))
}

resource "gitlab_project_hook" "webhook" {
  for_each = { for webhook in local.webhooks : "${webhook.project_index}:${webhook.url}" => webhook }

  project = gitlab_project.project[each.value.project_index].id

  url                        = each.value.url
  enable_ssl_verification    = each.value.enable_ssl_verification
  push_events_branch_filter  = each.value.push_events_branch_filter
  push_events                = each.value.push_events
  confidential_issues_events = each.value.confidential_issues_events
  confidential_note_events   = each.value.confidential_note_events
  deployment_events          = each.value.deployment_events
  issues_events              = each.value.issues_events
  job_events                 = each.value.job_events
  merge_requests_events      = each.value.merge_requests_events
  note_events                = each.value.note_events
  pipeline_events            = each.value.pipeline_events
  releases_events            = each.value.releases_events
  tag_push_events            = each.value.tag_push_events
  wiki_page_events           = each.value.wiki_page_events

  depends_on = [gitlab_project.project]
}
