data "gitlab_group" "group" {
  for_each = {
    for project in var.projects : keys(project)[0] => values(project)[0] if values(project)[0].namespace_id == null && values(project)[0].parent_group_full_path != ""
  }
  full_path = each.value.parent_group_full_path
}
