locals {
  deploy_keys = toset(flatten([
    for project in var.projects : [
      for deploy_key in values(project)[0].deploy_keys : merge(
        { project_index = keys(project)[0] },
      deploy_key)
    ]
  ]))
}

resource "gitlab_deploy_key_enable" "key" {
  for_each = { for deploy_key in local.deploy_keys : "${deploy_key.project_index}:${deploy_key.key_id}" => deploy_key }

  project  = gitlab_project.project[each.value.project_index].id
  can_push = each.value.can_push
  key_id   = each.value.key_id

  depends_on = [gitlab_project.project]
}
